using System.Collections.Immutable;
using Ignis.Nuke.GitLab.Packages;
using Nuke.Common.Tooling;

// ReSharper disable once CheckNamespace
namespace Ignis.Nuke.GitLab;

public static partial class GitLabTasks
{
    public static Task<ImmutableArray<Output>> GitLabPublishGenericPackageAsync(
        Configure<GitLabGenericPackageSettings> configure)
    {
        return GitLabPublishGenericPackageAsync(configure(new GitLabGenericPackageSettings()));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public static async Task<ImmutableArray<Output>> GitLabPublishGenericPackageAsync(
        GitLabGenericPackageSettings settings)
    {
        using var client = settings.GitLabClient();
        await client.Projects[settings.ProjectId].Packages.Generic
            .Packages[settings.PackageName]
            .Versions[settings.PackageVersion]
            .Files[settings.FileName]
            .PublishAsync(settings.Content);

        return ImmutableArray<Output>.Empty;
    }
}
