using Ignis.Nuke.GitLab.Projects;

namespace Ignis.Nuke.GitLab.Packages;

public sealed class GitLabGenericPackageSettings : GitLabProjectSettings
{
    public string PackageName { get; set; } = null!;
    public string PackageVersion { get; set; } = null!;
    public string FileName { get; set; } = null!;
    public byte[] Content { get; set; } = { };
}
