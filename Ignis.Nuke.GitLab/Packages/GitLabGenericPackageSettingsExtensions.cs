namespace Ignis.Nuke.GitLab.Packages;

public static class GitLabGenericPackageSettingsExtensions
{
    public static GitLabGenericPackageSettings SetPackageName(this GitLabGenericPackageSettings settings, string value)
    {
        settings.PackageName = value;
        return settings;
    }

    public static GitLabGenericPackageSettings SetPackageVersion(this GitLabGenericPackageSettings settings,
        string value)
    {
        settings.PackageVersion = value;
        return settings;
    }

    public static GitLabGenericPackageSettings SetFileName(this GitLabGenericPackageSettings settings, string value)
    {
        settings.FileName = value;
        return settings;
    }

    public static GitLabGenericPackageSettings SetContent(this GitLabGenericPackageSettings settings, byte[] value)
    {
        settings.Content = value;
        return settings;
    }
}
