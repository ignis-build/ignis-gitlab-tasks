using Ignis.Nuke.GitLab.Projects;

namespace Ignis.Nuke.GitLab.Releases;

public abstract class GitLabReleaseSettings : GitLabProjectSettings
{
    public string TagName { get; set; } = null!;
}
