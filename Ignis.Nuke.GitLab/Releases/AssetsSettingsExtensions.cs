using Ignis.GitLab.Api.Client.Releases;

namespace Ignis.Nuke.GitLab.Releases;

public static class AssetsSettingsExtensions
{
    public static AssetsSettings AddLink(this AssetsSettings settings, AssetsLink link)
    {
        settings.Links = settings.Links.Add(link);
        return settings;
    }

    public static AssetsSettings AddLinks(this AssetsSettings settings, IEnumerable<AssetsLink> links)
    {
        foreach (var link in links)
        {
            settings.AddLink(link);
        }

        return settings;
    }
}
