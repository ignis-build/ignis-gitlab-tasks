using Nuke.Common.Tooling;

namespace Ignis.Nuke.GitLab.Releases;

public static class GitLabCreateReleaseSettingsExtensions
{
    public static GitLabCreateReleaseSettings SetReleaseName(this GitLabCreateReleaseSettings settings, string value)
    {
        settings.ReleaseName = value;
        return settings;
    }

    public static GitLabCreateReleaseSettings SetReleasedAt(this GitLabCreateReleaseSettings settings, DateTime value)
    {
        settings.ReleasedAt = value;
        return settings;
    }

    public static GitLabCreateReleaseSettings SetDescription(this GitLabCreateReleaseSettings settings, string value)
    {
        settings.Description = value;
        return settings;
    }

    public static GitLabCreateReleaseSettings SetRef(this GitLabCreateReleaseSettings settings, string value)
    {
        settings.Ref = value;
        return settings;
    }

    public static GitLabCreateReleaseSettings AddMilestone(this GitLabCreateReleaseSettings settings, string value)
    {
        settings.Milestones.Add(value);
        return settings;
    }

    public static GitLabCreateReleaseSettings SetAssets(this GitLabCreateReleaseSettings settings,
        Configure<AssetsSettings> configure)
    {
        configure(settings.Assets);
        return settings;
    }
}
