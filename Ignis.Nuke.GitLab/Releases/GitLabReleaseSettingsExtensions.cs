namespace Ignis.Nuke.GitLab.Releases;

public static class GitLabReleaseSettingsExtensions
{
    public static T SetTagName<T>(this T settings, string value)
        where T : GitLabReleaseSettings
    {
        settings.TagName = value;
        return settings;
    }
}
