using Ignis.GitLab.Api.Client.Releases;

namespace Ignis.Nuke.GitLab.Releases;

public sealed class AssetsSettings
{
    public AssetsLinks Links { get; internal set; } = new();

    // ReSharper disable once MemberCanBePrivate.Global
    public Assets ToAssets()
    {
        return new Assets(Links);
    }

    public static implicit operator Assets(AssetsSettings settings)
    {
        return settings.ToAssets();
    }
}
