using Ignis.GitLab.Api.Client.Projects;
using Ignis.Nuke.GitLab.Common;

namespace Ignis.Nuke.GitLab.Projects;

public abstract class GitLabProjectSettings : GitLabSettings
{
    public ProjectId ProjectId { get; set; } = null!;
}
