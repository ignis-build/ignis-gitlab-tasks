using System.Collections.Immutable;
using Ignis.Nuke.GitLab.Version;
using Nuke.Common.Tooling;

// ReSharper disable once CheckNamespace
namespace Ignis.Nuke.GitLab;

public static partial class GitLabTasks
{
    public static Task<ImmutableArray<Output>> GitLabVersionAsync(Configure<GitLabVersionSettings> configure)
    {
        return GitLabVersionAsync(configure(new GitLabVersionSettings()));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public static async Task<ImmutableArray<Output>> GitLabVersionAsync(GitLabVersionSettings settings)
    {
        using var client = settings.GitLabClient();
        await client.Version.GetAsync();

        return ImmutableArray<Output>.Empty;
    }
}
