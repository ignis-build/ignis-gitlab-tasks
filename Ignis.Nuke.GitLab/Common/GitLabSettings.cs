using Ignis.GitLab.Api.Client;
using Ignis.GitLab.Api.Client.Authentication;

namespace Ignis.Nuke.GitLab.Common;

public abstract class GitLabSettings
{
    public Uri GitLabUri { get; set; } = new("https://gitlab.com");
    public Func<HttpClient> HttpClient { get; set; } = () => new HttpClient();
    public Credential Credential { get; set; } = Credential.Default();

    public GitLabApiClient GitLabClient()
    {
        var httpClient = HttpClient();
        httpClient.BaseAddress = GitLabUri;
        return new GitLabApiClient(httpClient, GitLabUri, Credential);
    }
}
