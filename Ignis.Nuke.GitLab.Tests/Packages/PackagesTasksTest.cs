using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Ignis.Nuke.GitLab.Common;
using Ignis.Nuke.GitLab.Projects;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.Nuke.GitLab.GitLabTasks;

namespace Ignis.Nuke.GitLab.Packages;

public sealed class PackagesTasksTest
{
    private readonly Func<HttpClient> _client;
    private readonly MockHttpClient _mocks;

    public PackagesTasksTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = () => _mocks.CreateHttpClient(output);
    }

    [Fact]
    public async Task TestGitLabPublishGenericPackageAsync()
    {
        _mocks.Setup(Put("https://gitlab.com/api/v4/projects/foo%2Fbar/packages/generic/package1/1.0.2/file1.zip",
            "hello"));

        await GitLabPublishGenericPackageAsync(s => s
            .SetHttpClient(_client)
            .SetProjectId("foo/bar")
            .SetPackageName("package1")
            .SetPackageVersion("1.0.2")
            .SetFileName("file1.zip")
            .SetContent(Encoding.UTF8.GetBytes("hello")));
    }
}
