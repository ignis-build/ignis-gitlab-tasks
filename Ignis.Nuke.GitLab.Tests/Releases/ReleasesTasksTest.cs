using System;
using System.Net.Http;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Ignis.GitLab.Api.Client.Releases;
using Ignis.Nuke.GitLab.Common;
using Ignis.Nuke.GitLab.Projects;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.Nuke.GitLab.GitLabTasks;

namespace Ignis.Nuke.GitLab.Releases;

public sealed class ReleasesTasksTest
{
    private readonly Func<HttpClient> _client;
    private readonly MockHttpClient _mocks;
    private readonly DateTime _releasedAt;

    public ReleasesTasksTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = () => _mocks.CreateHttpClient(output);

        _releasedAt = DateTime.Parse("2021-01-24T12:25:00.000+09:00");
    }

    [Fact]
    public async Task TestCreateReleaseAsync()
    {
        var request = new CreateReleaseRequest(
            "v1.0.1",
            "1.0.1",
            "description",
            "ref",
            _releasedAt,
            new Milestones(new[] {"1.0.0-milestone"}),
            new Assets(new AssetsLinks(new[]
            {
                new AssetsLink("foo", "https://example.jp/foo")
            })));
        _mocks.Setup(Post("https://gitlab.com/api/v4/projects/foo%2Fbar/releases", request));

        await GitLabCreateReleaseAsync(s => s
            .SetHttpClient(_client)
            .SetProjectId("foo/bar")
            .SetReleaseName("1.0.1")
            .SetTagName("v1.0.1")
            .SetDescription("description")
            .SetRef("ref")
            .SetReleasedAt(_releasedAt)
            .AddMilestone("1.0.0-milestone")
            .SetAssets(assets => assets
                .AddLink(new AssetsLink("foo", "https://example.jp/foo"))));
    }

    [Fact]
    public async Task DeleteReleaseAsync()
    {
        _mocks.Setup(Delete("https://gitlab.com/api/v4/projects/foo%2Fbar/releases/v1.0.0"));

        await GitLabDeleteReleaseAsync(s => s
            .SetHttpClient(_client)
            .SetProjectId("foo/bar")
            .SetTagName("v1.0.0"));
    }
}
