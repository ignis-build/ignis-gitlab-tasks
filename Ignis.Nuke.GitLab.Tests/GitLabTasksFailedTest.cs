using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Ignis.Nuke.GitLab.Common;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.GitLab.Api.Client.Http.Mocks.Response.MockHttpResponse;

namespace Ignis.Nuke.GitLab;

public sealed class GitLabTasksFailedTest
{
    private readonly Func<HttpClient> _client;
    private readonly MockHttpClient _mocks;

    public GitLabTasksFailedTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = () => _mocks.CreateHttpClient(output);
    }

    [Fact]
    public async Task TestFailed()
    {
        _mocks.Setup(Get("https://gitlab.com/api/v4/version"), Fail(HttpStatusCode.InternalServerError));

        await PAssert.Throws<HttpRequestException>(() => GitLabTasks.GitLabVersionAsync(s => s.SetHttpClient(_client)));
    }
}
