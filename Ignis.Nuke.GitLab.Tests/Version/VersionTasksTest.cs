using System;
using System.Net.Http;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Ignis.GitLab.Api.Client.Version;
using Ignis.Nuke.GitLab.Common;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.GitLab.Api.Client.Http.Mocks.Response.MockHttpResponse;

namespace Ignis.Nuke.GitLab.Version;

public sealed class VersionTasksTest
{
    private readonly Func<HttpClient> _client;
    private readonly MockHttpClient _mocks;

    public VersionTasksTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = () => _mocks.CreateHttpClient(output);
    }

    [Fact]
    public async Task TestVersionAsync()
    {
        _mocks.Setup(Get("https://gitlab.com/api/v4/version"), Json(new GitLabVersion("8.9.1-pre5", "")));

        await GitLabTasks.GitLabVersionAsync(s => s.SetHttpClient(_client));

        var actual = _mocks.HttpLogs();

        PAssert.IsTrue(() => actual.Contains("8.9.1-pre5"));
    }
}
