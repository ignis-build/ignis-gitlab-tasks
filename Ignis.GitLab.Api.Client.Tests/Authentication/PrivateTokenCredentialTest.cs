using System.Net.Http;
using PowerAssert;
using Xunit;

namespace Ignis.GitLab.Api.Client.Authentication;

public sealed class PrivateTokenCredentialTest
{
    [Fact]
    public void TestInjectTo()
    {
        using var client = new HttpClient();
        var target = Credential.PrivateToken("private-token");
        target.InjectTo(client);

        // ReSharper disable once AccessToDisposedClosure
        PAssert.IsTrue(() => client.DefaultRequestHeaders.PrivateToken() == "private-token");
    }
}
