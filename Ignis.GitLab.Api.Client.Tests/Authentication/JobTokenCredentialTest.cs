using System.Collections.Generic;
using System.Net.Http;
using PowerAssert;
using Xunit;

namespace Ignis.GitLab.Api.Client.Authentication;

public sealed class JobTokenCredentialTest
{
    private readonly Dictionary<string, string> _variables;

    public JobTokenCredentialTest()
    {
        _variables = new Dictionary<string, string>
        {
            {"CI_JOB_TOKEN", "job-token"}
        };
    }

    [Fact]
    public void TestTryGetFromEnvironmentVariables()
    {
        var target = JobTokenCredential.TryGet(_variables);

        PAssert.IsTrue(() => target != null);
    }

    [Fact]
    public void TestTryGetNoEnvironmentVariables()
    {
        var target = JobTokenCredential.TryGet(new Dictionary<string, string>());

        PAssert.IsTrue(() => target == null);
    }

    [Fact]
    public void TestInjectTo()
    {
        using var client = new HttpClient();
        var target = JobTokenCredential.TryGet(_variables)!;

        target.InjectTo(client);

        // ReSharper disable once AccessToDisposedClosure
        PAssert.IsTrue(() => client.DefaultRequestHeaders.JobToken() == "job-token");
    }
}
