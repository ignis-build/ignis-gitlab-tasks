using System.Linq;
using System.Net.Http.Headers;

namespace Ignis.GitLab.Api.Client.Authentication;

internal static class HttpRequestHeadersExtensions
{
    public static string? JobToken(this HttpRequestHeaders headers)
    {
        return headers.GetValue("JOB-TOKEN");
    }

    public static string? PrivateToken(this HttpRequestHeaders headers)
    {
        return headers.GetValue("PRIVATE-TOKEN");
    }

    private static string? GetValue(this HttpHeaders headers, string name)
    {
        return headers.GetValues(name).FirstOrDefault();
    }
}
