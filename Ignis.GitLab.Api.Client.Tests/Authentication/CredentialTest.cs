using System.Collections.Generic;
using PowerAssert;
using Xunit;

namespace Ignis.GitLab.Api.Client.Authentication;

public sealed class CredentialTest
{
    [Fact]
    public void TestDefaultNoCredential()
    {
        var actual = Credential.Default(new Dictionary<string, string>());

        PAssert.IsTrue(() => actual is NoCredential);
    }

    [Fact]
    public void TestDefaultJobTokenCredential()
    {
        var actual = Credential.Default(new Dictionary<string, string>
        {
            {"CI_JOB_TOKEN", "job-token"}
        });

        PAssert.IsTrue(() => actual is JobTokenCredential);
    }
}
