using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Ignis.GitLab.Api.Client.Http.Mocks.Response;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;

namespace Ignis.GitLab.Api.Client.Version;

public sealed class GitLabVersionApiClientTest
{
    private readonly MockHttpClient _mocks;
    private readonly GitLabApiClient _target;

    public GitLabVersionApiClientTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _target = new GitLabApiClient(_mocks.CreateHttpClient(output));
    }

    [Fact]
    public async Task TestGetAsync()
    {
        var version = new GitLabVersion("8.13.0-pre", "4e963fe");

        _mocks.Setup(Get("https://gitlab.com/api/v4/version"), MockHttpResponse.Json(version));

        var actual = await _target.Version.GetAsync();

        PAssert.IsTrue(() => actual == version);
    }
}
