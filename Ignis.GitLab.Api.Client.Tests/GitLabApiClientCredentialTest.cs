using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Authentication;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Ignis.GitLab.Api.Client.Version;
using PowerAssert;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;
using static Ignis.GitLab.Api.Client.Http.Mocks.Response.MockHttpResponse;

namespace Ignis.GitLab.Api.Client;

public sealed class GitLabApiClientCredentialTest : IDisposable
{
    private readonly HttpClient _client;
    private readonly MockHttpClient _mocks;

    public GitLabApiClientCredentialTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = _mocks.CreateHttpClient(output);
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestPrivateToken()
    {
        _mocks.Setup(Get("https://gitlab.com/api/v4/version"),
            Json(new GitLabVersion("8.13.0-pre", "4e963fe")));

        var client = new GitLabApiClient(_client, credential: Credential.PrivateToken("private-token"));

        await client.Version.GetAsync();

        var actual = _mocks.HttpLogs();

        PAssert.IsTrue(() => actual.Contains("PRIVATE-TOKEN: private-token"));
    }

    [Fact]
    public async Task TestJobToken()
    {
        _mocks.Setup(Get("https://gitlab.com/api/v4/version"),
            Json(new GitLabVersion("8.13.0-pre", "4e963fe")));

        var client = new GitLabApiClient(_client, new Dictionary<string, string>
        {
            {"CI_JOB_TOKEN", "job-token"}
        });

        await client.Version.GetAsync();

        var actual = _mocks.HttpLogs();

        PAssert.IsTrue(() => actual.Contains("JOB-TOKEN: job-token"));
    }
}
