using PowerAssert;
using Xunit;

namespace Ignis.GitLab.Api.Client.Http;

public sealed class ApiPathTest
{
    [Fact]
    public void TestEquals()
    {
        var left = new ApiPath("projects/");
        var right = new ApiPath("projects/");

        PAssert.IsTrue(() => left == right);
    }

    [Fact]
    public void TestNotEquals()
    {
        var left = new ApiPath("projects");
        var right = new ApiPath("release");

        PAssert.IsTrue(() => left != right);
    }

    [Fact]
    public void TestToString()
    {
        var target = new ApiPath("projects");

        PAssert.IsTrue(() => target.ToString() == "projects");
    }

    [Fact]
    public void TestAppend()
    {
        var target = new ApiPath("projects") / "12";

        PAssert.IsTrue(() => target.ToString() == "projects/12");
    }

    [Fact]
    public void TestProjectApiPath()
    {
        // ReSharper disable once VariantPathSeparationHighlighting
        var target = new ApiPath("projects") / "foo/bar";

        // ReSharper disable once StringLiteralTypo
        PAssert.IsTrue(() => target.ToString() == "projects/foo%2Fbar");
    }

    [Fact]
    public void TestV4EndPoint()
    {
        PAssert.IsTrue(() => ApiPath.V4.ToString() == "api/v4");
    }
}
