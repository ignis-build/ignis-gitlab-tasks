using System.Text;
using PowerAssert;
using Xunit;

namespace Ignis.GitLab.Api.Client.Http;

public sealed class UploadDataTest
{
    [Fact]
    public void TestEquals()
    {
        var left = new UploadData(new byte[] {1, 2, 3});
        var right = new UploadData(new byte[] {1, 2, 3});

        PAssert.IsTrue(() => left == right);
    }

    [Fact]
    public void TestNotEquals()
    {
        var left = new UploadData(new byte[] {1, 2, 3});
        var right = new UploadData(new byte[] {3, 2, 1});

        PAssert.IsTrue(() => left != right);
    }

    [Fact]
    public void TestFromBytes()
    {
        UploadData actual = new byte[] {1, 2, 3};

        PAssert.IsTrue(() => actual == new UploadData(new byte[] {1, 2, 3}));
    }

    [Fact]
    public void TestFromString()
    {
        UploadData actual = "hello";
        var expected = new UploadData(Encoding.UTF8.GetBytes("hello"));

        PAssert.IsTrue(() => actual == expected);
    }

    [Fact]
    public void TestToString()
    {
        UploadData target = "hello";

        PAssert.IsTrue(() => $"{target}" == "hello");
    }

    [Fact]
    public void TestToStringBytes()
    {
        var target = new UploadData(new byte[] {1, 2, 3});

        PAssert.IsTrue(() => $"{target}" == "3 bytes binary.");
    }
}
