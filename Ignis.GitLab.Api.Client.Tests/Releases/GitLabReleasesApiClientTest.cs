using System;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;

namespace Ignis.GitLab.Api.Client.Releases;

public sealed class GitLabReleasesApiClientTest : IDisposable
{
    private readonly GitLabApiClient _client;
    private readonly MockHttpClient _mocks;
    private readonly GitLabReleasesApiClient _target;

    public GitLabReleasesApiClientTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = new GitLabApiClient(_mocks.CreateHttpClient(output));

        _target = _client.Projects["foo/bar"].Releases;
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestCreateAsync()
    {
        var request = new CreateReleaseRequest(
            "v1.0.0",
            "1.0.0",
            "description",
            "ref",
            DateTime.Parse("2022-01-24T16:09:00+0900"));
        _mocks.Setup(Post("https://gitlab.com/api/v4/projects/foo%2Fbar/releases", request));

        await _target.CreateAsync(request);
    }

    [Fact]
    public async Task TestCreateWithMilestonesAsync()
    {
        var request = new CreateReleaseRequest("v1.0.0", Milestones: new Milestones(new[] {"v1.0.0"}));
        _mocks.Setup(Post("https://gitlab.com/api/v4/projects/foo%2Fbar/releases", request));

        await _target.CreateAsync(request);
    }

    [Fact]
    public async Task TestCreateWithAssetsAsync()
    {
        var request = new CreateReleaseRequest("v1.0.0", Assets: new Assets(new AssetsLinks(new[]
        {
            new AssetsLink("bar", "https://example.jp/bar", "bar.zip", "other")
        })));
        _mocks.Setup(Post("https://gitlab.com/api/v4/projects/foo%2Fbar/releases", request));

        await _target.CreateAsync(request);
    }

    [Fact]
    public async Task TestDeleteAsync()
    {
        _mocks.Setup(Delete("https://gitlab.com/api/v4/projects/foo%2Fbar/releases/v1.0.0"));

        await _client.Projects["foo/bar"].Releases["v1.0.0"].DeleteAsync();
    }
}
