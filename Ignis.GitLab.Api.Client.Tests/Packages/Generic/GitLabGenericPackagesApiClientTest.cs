using System;
using System.Threading.Tasks;
using Ignis.GitLab.Api.Client.Http.Mocks;
using Xunit;
using Xunit.Abstractions;
using static Ignis.GitLab.Api.Client.Http.Mocks.Request.MockHttpRequest;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class GitLabGenericPackagesApiClientTest : IDisposable
{
    private readonly GitLabApiClient _client;
    private readonly MockHttpClient _mocks;
    private readonly GitLabGenericPackageApiClient _target;

    public GitLabGenericPackagesApiClientTest(ITestOutputHelper output)
    {
        _mocks = new MockHttpClient();
        _client = new GitLabApiClient(_mocks.CreateHttpClient(output));
        _target = _client.Projects["foo/bar"].Packages.Generic;
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestPublishAsync()
    {
        _mocks.Setup(Put("https://gitlab.com/api/v4/projects/foo%2Fbar/packages/generic/package1/1.0.2/file1.zip",
            "hello"));

        await _target.Packages["package1"].Versions["1.0.2"].Files["file1.zip"].PublishAsync("hello");
    }
}
