using System.IO;
using Ignis.Nuke.GitLab.Common;
using Ignis.Nuke.GitLab.Packages;
using Ignis.Nuke.GitLab.Projects;
using Nuke.Common;
using Nuke.Common.Tools.DotNet;
using static Ignis.Nuke.GitLab.GitLabTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface IUpload : ICompile, IGitLab
{
    Target Publish => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetPublish(s => s
                .SetProject(Solution.GetProject("Ignis.Nuke.GitLab"))
                .SetConfiguration(Configuration)
                .SetOutput(OutputDirectory / "ignis-gitlab-tasks")
                .EnableNoRestore()
                .EnableNoBuild());
        });

    // ReSharper disable once UnusedMember.Global
    Target Upload => _ => _
        .DependsOn(Publish)
        .Executes(async () =>
        {
            await GitLabPublishGenericPackageAsync(s => s
                .SetCredential(GitLabCredential())
                .SetProjectId(GitLabProject)
                .SetPackageName("package1")
                .SetPackageVersion("1.0.1")
                .SetFileName("Ignis.Nuke.GitLab.dll")
                .SetContent(File.ReadAllBytes(OutputDirectory / "ignis-gitlab-tasks" / "Ignis.Nuke.GitLab.dll")));
        });
}
