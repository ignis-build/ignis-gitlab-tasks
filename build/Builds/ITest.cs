using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

public interface ITest : ICompile
{
    private AbsolutePath JunitReportFile => ReportDirectory / "junit" / "{assembly}-test-result.xml";

    // ReSharper disable once UnusedMember.Global
    Target Test => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            DotNetTest(s => s
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .EnableNoBuild()
                .EnableNoRestore()
                .AddLoggers(@$"junit;LogFilePath={JunitReportFile};MethodFormat=Class;FailureBodyFormat=Verbose"));
        });
}
