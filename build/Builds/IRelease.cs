using System.Linq;
using Ignis.GitLab.Api.Client.Releases.NuGet;
using Ignis.Nuke.GitLab.Common;
using Ignis.Nuke.GitLab.Projects;
using Ignis.Nuke.GitLab.Releases;
using Nuke.Common;
using Nuke.Common.CI.GitLab;
using static Ignis.Nuke.GitLab.GitLabTasks;

// ReSharper disable StringLiteralTypo

namespace Builds;

interface IRelease : IGitLab, IPush
{
    [Parameter("Git repository tag name. Default is loaded from `CI_COMMIT_REF_NAME` environment variables.")]
    string GitTag => TryGetValue(() => GitTag) ?? GitLab.Instance.CommitRefName;

    // ReSharper disable once UnusedMember.Global
    Target Release => _ => _
        .DependsOn(Push)
        .Executes(async () =>
        {
            var assetsLinks = new NuGetAssetLinkBuilder()
                .UseNuGetOrg()
                .Version(GitVersion.NuGetVersionV2)
                .AddPackages(PackagedProjects.Select(p => p.Name))
                .Build();

            await GitLabCreateReleaseAsync(s => s
                .SetProjectId(GitLabProject)
                .SetCredential(GitLabCredential())
                .SetTagName(GitTag)
                .SetAssets(assets => assets.AddLinks(assetsLinks)));
        });
}
