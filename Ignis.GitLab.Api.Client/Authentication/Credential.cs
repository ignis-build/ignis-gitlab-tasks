using System.Collections;

namespace Ignis.GitLab.Api.Client.Authentication;

/// <summary>
///     GitLab API credential.
/// </summary>
public abstract class Credential
{
    internal abstract void InjectTo(HttpClient client);

    public static Credential PrivateToken(string token)
    {
        return new PrivateTokenCredential(token);
    }

    public static Credential Default()
    {
        return Default(Environment.GetEnvironmentVariables());
    }

    internal static Credential Default(IDictionary variables)
    {
        return JobTokenCredential.TryGet(variables) ?? NoCredential.Instance;
    }
}
