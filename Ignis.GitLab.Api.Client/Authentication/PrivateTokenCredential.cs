using System.ComponentModel;
using System.Globalization;

namespace Ignis.GitLab.Api.Client.Authentication;

[TypeConverter(typeof(TypeConverter))]
public sealed class PrivateTokenCredential : Credential
{
    private readonly string _token;

    public PrivateTokenCredential(string token)
    {
        _token = token;
    }

    internal override void InjectTo(HttpClient client)
    {
        client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", _token);
    }

    private sealed class TypeConverter : System.ComponentModel.TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return new PrivateTokenCredential($"{value}");
        }
    }
}
