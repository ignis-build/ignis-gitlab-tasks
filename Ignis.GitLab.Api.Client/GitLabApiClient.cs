using System.Collections;
using Ignis.GitLab.Api.Client.Authentication;
using Ignis.GitLab.Api.Client.Http;
using Ignis.GitLab.Api.Client.Projects;
using Ignis.GitLab.Api.Client.Version;

namespace Ignis.GitLab.Api.Client;

public sealed class GitLabApiClient : IDisposable
{
    private readonly HttpClient _httpClient;
    private readonly Lazy<GitLabProjectsApiClient> _projects;
    private readonly Lazy<GitLabVersionApiClient> _version;

    internal GitLabApiClient(HttpClient httpClient, IDictionary variables) :
        this(httpClient, null, Credential.Default(variables))
    {
    }

    public GitLabApiClient(HttpClient httpClient,
        Uri? gitLabUri = null,
        Credential? credential = null)
    {
        (credential ?? Credential.Default()).InjectTo(httpClient);

        httpClient.BaseAddress = gitLabUri ?? new Uri("https://gitlab.com");
        _httpClient = httpClient;
        var client = new HttpApiClient(_httpClient);
        _projects = new Lazy<GitLabProjectsApiClient>(() => new GitLabProjectsApiClient(client));
        _version = new Lazy<GitLabVersionApiClient>(() => new GitLabVersionApiClient(client));
    }

    public GitLabProjectsApiClient Projects => _projects.Value;
    public GitLabVersionApiClient Version => _version.Value;

    public void Dispose()
    {
        _httpClient.Dispose();
    }
}
