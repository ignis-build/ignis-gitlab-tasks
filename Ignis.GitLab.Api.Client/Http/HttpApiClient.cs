using System.Net.Http.Json;

namespace Ignis.GitLab.Api.Client.Http;

internal sealed class HttpApiClient
{
    private readonly HttpClient _client;
    private readonly ApiPath _path;

    public HttpApiClient(HttpClient client) : this(client, ApiPath.V4)
    {
    }

    private HttpApiClient(HttpClient client, ApiPath path)
    {
        _client = client;
        _path = path;
    }

    public async Task<ApiResponse> GetAsync()
    {
        var response = await _client.GetAsync(_path);
        return ApiResponse.EnsureSuccess(response);
    }

    public async Task<ApiResponse> PostAsync(object body)
    {
        var content = JsonContent.Create(body);
        var response = await _client.PostAsync(_path, content);
        return ApiResponse.EnsureSuccess(response);
    }

    public async Task<ApiResponse> PutAsync(UploadData data)
    {
        var response = await _client.PutAsync(_path, data.ToContent());
        return ApiResponse.EnsureSuccess(response);
    }

    public async Task<ApiResponse> DeleteAsync()
    {
        var response = await _client.DeleteAsync(_path);
        return ApiResponse.EnsureSuccess(response);
    }

    public HttpApiClient Path(string path)
    {
        return Path((ApiPath) path);
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public HttpApiClient Path(ApiPath path)
    {
        return new HttpApiClient(_client, _path / path);
    }
}
