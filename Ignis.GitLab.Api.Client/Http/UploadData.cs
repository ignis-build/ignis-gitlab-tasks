namespace Ignis.GitLab.Api.Client.Http;

public class UploadData : IEquatable<UploadData>
{
    private readonly byte[] _data;

    public UploadData(byte[] data)
    {
        _data = data;
    }

    public bool Equals(UploadData? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _data.SequenceEqual(other._data);
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is UploadData other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _data.GetHashCode();
    }

    public static bool operator ==(UploadData? left, UploadData? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(UploadData? left, UploadData? right)
    {
        return !Equals(left, right);
    }

    public static implicit operator UploadData(byte[] data)
    {
        return new UploadData(data);
    }

    public static implicit operator UploadData(string data)
    {
        return new StringUploadData(data);
    }

    public override string ToString()
    {
        return $"{_data.Length} bytes binary.";
    }

    internal HttpContent ToContent()
    {
        return new ByteArrayContent(_data);
    }
}
