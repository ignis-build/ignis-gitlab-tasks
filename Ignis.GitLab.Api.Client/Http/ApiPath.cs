namespace Ignis.GitLab.Api.Client.Http;

internal sealed record ApiPath
{
    private readonly string _value;

    public ApiPath(string value)
    {
        _value = Normalize(value);
    }

    private ApiPath(ApiPath left, ApiPath right)
    {
        _value = $"{left}/{right}";
    }

    public static ApiPath V4 { get; } = new ApiPath("api") / "v4";

    private static string Normalize(string value)
    {
        return Uri.EscapeDataString(value);
    }

    public override string ToString()
    {
        return _value;
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public ApiPath Append(ApiPath path)
    {
        return new ApiPath(this, path);
    }

    public static ApiPath operator /(ApiPath left, ApiPath right)
    {
        return left.Append(right);
    }

    public static implicit operator ApiPath(string value)
    {
        return new ApiPath(value);
    }

    public static implicit operator string(ApiPath value)
    {
        return $"{value}";
    }
}
