using System.Net.Http.Json;

namespace Ignis.GitLab.Api.Client.Http;

internal sealed class ApiResponse
{
    private readonly HttpResponseMessage _response;

    private ApiResponse(HttpResponseMessage response)
    {
        _response = response;
    }

    public async Task<T> GetObjectAsync<T>()
    {
        return await _response.Content.ReadFromJsonAsync<T>() ?? throw new InvalidOperationException();
    }

    public static ApiResponse EnsureSuccess(HttpResponseMessage response)
    {
        return new ApiResponse(response.EnsureSuccessStatusCode());
    }
}
