using System.Text;

namespace Ignis.GitLab.Api.Client.Http;

internal sealed class StringUploadData : UploadData
{
    private readonly string _data;

    public StringUploadData(string data) : base(Encoding.UTF8.GetBytes(data))
    {
        _data = data;
    }

    public override string ToString()
    {
        return _data;
    }
}
