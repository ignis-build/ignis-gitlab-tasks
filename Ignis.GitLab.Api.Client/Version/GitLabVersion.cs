// ReSharper disable UnusedMember.Global

namespace Ignis.GitLab.Api.Client.Version;

public sealed record GitLabVersion(string Version, string Revision)
{
    public string Version { get; } = Version;
    public string Revision { get; } = Revision;
}
