using Ignis.GitLab.Api.Client.Http;
using Ignis.GitLab.Api.Client.Packages;
using Ignis.GitLab.Api.Client.Releases;

namespace Ignis.GitLab.Api.Client.Projects;

public sealed class GitLabProjectApiClient
{
    private readonly Lazy<GitLabPackageApiClient> _packages;
    private readonly Lazy<GitLabReleasesApiClient> _releases;

    internal GitLabProjectApiClient(HttpApiClient client, ProjectId projectId) : this(client.Path(projectId))
    {
    }

    private GitLabProjectApiClient(HttpApiClient client)
    {
        _releases = new Lazy<GitLabReleasesApiClient>(() => new GitLabReleasesApiClient(client));
        _packages = new Lazy<GitLabPackageApiClient>(() => new GitLabPackageApiClient(client));
    }

    public GitLabReleasesApiClient Releases => _releases.Value;
    public GitLabPackageApiClient Packages => _packages.Value;
}
