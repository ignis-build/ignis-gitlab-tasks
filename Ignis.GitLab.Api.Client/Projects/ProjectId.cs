namespace Ignis.GitLab.Api.Client.Projects;

/// <summary>
///     GitLab Project ID.(Ex: 12 or `foo/bar`)
/// </summary>
public sealed record ProjectId
{
    private readonly string _value;

    private ProjectId(string value)
    {
        _value = value;
    }

    public override string ToString()
    {
        return _value;
    }

    public static implicit operator ProjectId(string value)
    {
        return new ProjectId(value);
    }

    public static implicit operator string(ProjectId value)
    {
        return $"{value}";
    }
}
