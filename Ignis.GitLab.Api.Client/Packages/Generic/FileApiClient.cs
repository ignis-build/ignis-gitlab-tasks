using System.Text;
using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class FileApiClient
{
    private readonly HttpApiClient _client;

    internal FileApiClient(HttpApiClient client, string fileName)
    {
        _client = client.Path(fileName);
    }

    internal Task PublishAsync(string data)
    {
        return PublishAsync(Encoding.UTF8.GetBytes(data));
    }

    public async Task PublishAsync(byte[] data)
    {
        await _client.PutAsync(data);
    }
}
