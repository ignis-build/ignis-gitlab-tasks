using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class VersionApiClient
{
    private readonly Lazy<FilesApiClient> _files;

    internal VersionApiClient(HttpApiClient client, string packageVersion)
    {
        _files = new Lazy<FilesApiClient>(() => new FilesApiClient(client.Path(packageVersion)));
    }

    public FilesApiClient Files => _files.Value;
}
