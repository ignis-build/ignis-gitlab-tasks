using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class PackageApiClient
{
    private readonly Lazy<VersionsApiClient> _version;

    internal PackageApiClient(HttpApiClient client, string packageName)
    {
        _version = new Lazy<VersionsApiClient>(() => new VersionsApiClient(client.Path(packageName)));
    }

    public VersionsApiClient Versions => _version.Value;
}
