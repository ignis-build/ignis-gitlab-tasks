using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class VersionsApiClient
{
    private readonly HttpApiClient _client;

    internal VersionsApiClient(HttpApiClient client)
    {
        _client = client;
    }

    public VersionApiClient this[string packageVersion] => new(_client, packageVersion);
}
