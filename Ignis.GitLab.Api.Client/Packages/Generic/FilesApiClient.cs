using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class FilesApiClient
{
    private readonly HttpApiClient _client;

    internal FilesApiClient(HttpApiClient client)
    {
        _client = client;
    }

    public FileApiClient this[string fileName] => new(_client, fileName);
}
