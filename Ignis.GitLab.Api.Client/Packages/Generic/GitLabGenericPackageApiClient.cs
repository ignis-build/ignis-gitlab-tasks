using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Packages.Generic;

public sealed class GitLabGenericPackageApiClient
{
    private readonly Lazy<PackagesApiClient> _packages;

    internal GitLabGenericPackageApiClient(HttpApiClient client)
    {
        _packages = new Lazy<PackagesApiClient>(() => new PackagesApiClient(client.Path("generic")));
    }

    public PackagesApiClient Packages => _packages.Value;
}
