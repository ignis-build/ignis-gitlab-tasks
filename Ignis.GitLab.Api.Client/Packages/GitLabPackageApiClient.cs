using Ignis.GitLab.Api.Client.Http;
using Ignis.GitLab.Api.Client.Packages.Generic;

namespace Ignis.GitLab.Api.Client.Packages;

public sealed class GitLabPackageApiClient
{
    private readonly Lazy<GitLabGenericPackageApiClient> _generic;

    internal GitLabPackageApiClient(HttpApiClient client)
    {
        _generic = new Lazy<GitLabGenericPackageApiClient>(() =>
            new GitLabGenericPackageApiClient(client.Path("packages")));
    }

    public GitLabGenericPackageApiClient Generic => _generic.Value;
}
