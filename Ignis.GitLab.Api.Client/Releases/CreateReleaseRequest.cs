using System.Text.Json.Serialization;

// ReSharper disable UnusedMember.Global

namespace Ignis.GitLab.Api.Client.Releases;

public sealed record CreateReleaseRequest(
    string TagName,
    string? Name = null,
    string? Description = null,
    string? Ref = null,
    DateTime? ReleasedAt = null,
    Milestones? Milestones = null,
    Assets? Assets = null)
{
    [JsonPropertyName("name")] public string? Name { get; } = Name;
    [JsonPropertyName("tag_name")] public string TagName { get; } = TagName;
    [JsonPropertyName("description")] public string? Description { get; } = Description;
    [JsonPropertyName("ref")] public string? Ref { get; } = Ref;
    [JsonPropertyName("released_at")] public DateTime? ReleasedAt { get; } = ReleasedAt;
    [JsonPropertyName("milestones")] public Milestones? Milestones { get; } = Milestones;
    [JsonPropertyName("assets")] public Assets? Assets { get; } = Assets;
}
