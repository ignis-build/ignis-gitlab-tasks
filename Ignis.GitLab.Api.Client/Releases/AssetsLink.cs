using System.Text.Json.Serialization;

// ReSharper disable UnusedMember.Global

namespace Ignis.GitLab.Api.Client.Releases;

public sealed record AssetsLink(string Name, string Url, string? Filepath = null, string? LinkType = null)
{
    [JsonPropertyName("name")] public string Name { get; } = Name;
    [JsonPropertyName("url")] public string Url { get; } = Url;
    [JsonPropertyName("filepath")] public string? Filepath { get; } = Filepath;
    [JsonPropertyName("link_type")] public string? LinkType { get; } = LinkType;
}
