using Ignis.GitLab.Api.Client.Http;

namespace Ignis.GitLab.Api.Client.Releases;

public sealed class GitLabReleaseApiClient
{
    private readonly HttpApiClient _client;

    internal GitLabReleaseApiClient(HttpApiClient client, string tagName) : this(client.Path(tagName))
    {
    }

    private GitLabReleaseApiClient(HttpApiClient client)
    {
        _client = client;
    }

    public async Task DeleteAsync()
    {
        await _client.DeleteAsync();
    }
}
