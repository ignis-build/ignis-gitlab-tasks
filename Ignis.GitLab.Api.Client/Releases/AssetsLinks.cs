using System.Collections;
using System.Collections.Immutable;

namespace Ignis.GitLab.Api.Client.Releases;

public sealed class AssetsLinks : IEquatable<AssetsLinks>, IEnumerable<AssetsLink>
{
    private readonly ImmutableArray<AssetsLink> _items;

    public AssetsLinks() : this(Enumerable.Empty<AssetsLink>())
    {
    }

    public AssetsLinks(IEnumerable<AssetsLink> items)
    {
        _items = items.ToImmutableArray();
    }

    public IEnumerator<AssetsLink> GetEnumerator()
    {
        return ((IEnumerable<AssetsLink>) _items).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public bool Equals(AssetsLinks? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _items.SequenceEqual(other._items);
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is AssetsLinks other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _items.GetHashCode();
    }

    public AssetsLinks Add(AssetsLink link)
    {
        return new AssetsLinks(_items.Union(new[] {link}));
    }
}
