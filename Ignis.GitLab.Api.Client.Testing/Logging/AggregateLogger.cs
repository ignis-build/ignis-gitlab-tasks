using System.Collections.Immutable;

namespace Ignis.GitLab.Api.Client.Logging;

internal sealed class AggregateLogger : ILogger
{
    private readonly IEnumerable<ILogger> _loggers;

    public AggregateLogger(IEnumerable<ILogger> loggers)
    {
        _loggers = loggers.ToImmutableArray();
    }

    public void WriteLine(string text = "")
    {
        foreach (var logger in _loggers) logger.WriteLine(text);
    }
}
