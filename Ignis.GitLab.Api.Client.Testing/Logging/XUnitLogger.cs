using Xunit.Abstractions;

namespace Ignis.GitLab.Api.Client.Logging;

internal sealed class XUnitLogger : ILogger
{
    private readonly ITestOutputHelper _output;

    public XUnitLogger(ITestOutputHelper output)
    {
        _output = output;
    }

    public void WriteLine(string text)
    {
        _output.WriteLine(text);
    }
}
