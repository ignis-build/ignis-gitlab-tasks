using System.Text;

namespace Ignis.GitLab.Api.Client.Logging;

internal sealed class RecordLogger : ILogger
{
    private readonly StringBuilder _logs = new();

    public void WriteLine(string text = "")
    {
        _logs.AppendLine(text);
    }

    public override string ToString()
    {
        return _logs.ToString();
    }
}
