using System.Net.Http.Headers;

namespace Ignis.GitLab.Api.Client.Http.Logging;

internal static class HttpHeadersExtensions
{
    public static IEnumerable<HttpHeader> Flatten(this HttpHeaders headers)
    {
        return headers.SelectMany(header => header.Value, (header, value) => new HttpHeader(header.Key, value));
    }
}
