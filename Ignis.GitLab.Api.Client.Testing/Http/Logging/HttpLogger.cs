using System.Net.Http.Headers;
using Ignis.GitLab.Api.Client.Logging;

namespace Ignis.GitLab.Api.Client.Http.Logging;

internal sealed class HttpLogger
{
    private readonly ILogger _logger;

    public HttpLogger(ILogger logger)
    {
        _logger = logger;
    }

    public async Task WriteAsync(HttpRequestMessage request)
    {
        _logger.WriteLine($"{request.Method} {request.RequestUri} HTTP/{request.Version}");
        WriteHeadersAsync(request.Headers);

        if (request.Content == null) return;

        WriteHeadersAsync(request.Content.Headers);

        _logger.WriteLine();

        _logger.WriteLine(await request.Content.ReadAsStringAsync());
    }

    public async Task WriteAsync(HttpResponseMessage response)
    {
        _logger.WriteLine();
        WriteHeadersAsync(response.Content.Headers);
        _logger.WriteLine();
        _logger.WriteLine(await response.Content.ReadAsStringAsync());
    }

    private void WriteHeadersAsync(HttpHeaders headers)
    {
        foreach (var header in headers.Flatten()) _logger.WriteLine(header.ToString());
    }
}
