namespace Ignis.GitLab.Api.Client.Http.Logging;

internal sealed record HttpHeader(string Key, string Value)
{
    public override string ToString()
    {
        return $"{Key}: {Value}";
    }
}
