using System.Net;
using Ignis.GitLab.Api.Client.Http.Mocks.Request;
using Ignis.GitLab.Api.Client.Http.Mocks.Response;
using Ignis.GitLab.Api.Client.Logging;
using Xunit.Abstractions;

namespace Ignis.GitLab.Api.Client.Http.Mocks;

public sealed class MockHttpClient
{
    private readonly RecordLogger _logger = new();
    private readonly MockHttpProcessCollection _processes = new();

    public void Setup(MockHttpRequest request, MockHttpResponse? response = null)
    {
        _processes.Add(request, response ?? MockHttpResponse.Empty);
    }

    public HttpClient CreateHttpClient(ITestOutputHelper output)
    {
        var logger = new AggregateLogger(new ILogger[]
        {
            new XUnitLogger(output),
            _logger
        });

        return new HttpClient(CreateHttpMessageHandler(logger));
    }

    private HttpMessageHandler CreateHttpMessageHandler(ILogger logger)
    {
        return new MockHttpMessageHandler(this, logger);
    }

    public async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
    {
        return await SendAsync(await MockHttpRequest.FromAsync(request));
    }

    public string HttpLogs()
    {
        return _logger.ToString();
    }

    private Task<HttpResponseMessage> SendAsync(MockHttpRequest request)
    {
        var process = _processes.Find(request);

        var message = new HttpResponseMessage(HttpStatusCode.OK)
        {
            StatusCode = process.GetResponseCode(),
            Content = process.GetResponseContent()
        };

        return Task.FromResult(message);
    }
}
