using System.Net;
using Ignis.GitLab.Api.Client.Http.Mocks.Request;
using Ignis.GitLab.Api.Client.Http.Mocks.Response;

namespace Ignis.GitLab.Api.Client.Http.Mocks;

internal sealed class MockHttpProcess
{
    public MockHttpProcess(MockHttpRequest request, MockHttpResponse response)
    {
        Request = request;
        Response = response;
    }

    private MockHttpRequest Request { get; }
    private MockHttpResponse Response { get; }

    public bool Match(MockHttpRequest request)
    {
        return Request == request;
    }

    public HttpContent? GetResponseContent()
    {
        return Response.ToContent();
    }

    public HttpStatusCode GetResponseCode()
    {
        return Response.GetResponseCode();
    }
}
