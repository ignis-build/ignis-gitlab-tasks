using Ignis.GitLab.Api.Client.Http.Mocks.Request;
using Ignis.GitLab.Api.Client.Http.Mocks.Response;

namespace Ignis.GitLab.Api.Client.Http.Mocks;

internal sealed class MockHttpProcessCollection
{
    private readonly List<MockHttpProcess> _processes = new();

    public void Add(MockHttpRequest request, MockHttpResponse response)
    {
        _processes.Add(new MockHttpProcess(request, response));
    }

    public MockHttpProcess Find(MockHttpRequest request)
    {
        var process = _processes.FirstOrDefault(process => process.Match(request));
        if (process == null) throw new InvalidOperationException($"`{request.Method} {request.Uri}` is not defined.");

        return process;
    }
}
