using System.Text.Json;

namespace Ignis.GitLab.Api.Client.Http.Mocks.Request;

// ReSharper disable once NotAccessedPositionalProperty.Global
public sealed record MockHttpRequest
{
    private static readonly JsonSerializerOptions SerializerOptions = new()
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase
    };

    private MockHttpRequest(HttpMethod method, string uri, UploadData? body = null) : this(method, new Uri(uri), body)
    {
    }

    private MockHttpRequest(HttpMethod method, Uri uri, UploadData? body = null)
    {
        Method = method;
        Uri = uri;
        Body = body;
    }

    public HttpMethod Method { get; }
    public Uri Uri { get; }

    // ReSharper disable once UnusedAutoPropertyAccessor.Global
    // ReSharper disable once MemberCanBePrivate.Global
    public UploadData? Body { get; }

    public static MockHttpRequest Get(string uri)
    {
        return new MockHttpRequest(HttpMethod.Get, uri);
    }

    public static MockHttpRequest Post(string uri, object body)
    {
        return new MockHttpRequest(HttpMethod.Post, uri, Json(body));
    }

    public static MockHttpRequest Put(string uri, UploadData data)
    {
        return new MockHttpRequest(HttpMethod.Put, uri, data);
    }

    public static MockHttpRequest Delete(string uri)
    {
        return new MockHttpRequest(HttpMethod.Delete, uri);
    }

    private static UploadData? Json(object? body)
    {
        if (body == null) return null;
        return JsonSerializer.Serialize(body, SerializerOptions);
    }

    public static async Task<MockHttpRequest> FromAsync(HttpRequestMessage request)
    {
        return new MockHttpRequest(request.Method, request.RequestUri!, await GetBodyAsync(request.Content));
    }

    private static async Task<UploadData?> GetBodyAsync(HttpContent? content)
    {
        if (content == null) return null;
        return await content.ReadAsByteArrayAsync();
    }
}
