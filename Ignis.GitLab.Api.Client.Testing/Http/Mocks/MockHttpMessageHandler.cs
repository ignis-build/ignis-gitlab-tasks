using Ignis.GitLab.Api.Client.Http.Logging;
using Ignis.GitLab.Api.Client.Logging;

namespace Ignis.GitLab.Api.Client.Http.Mocks;

internal sealed class MockHttpMessageHandler : HttpMessageHandler
{
    private readonly HttpLogger _logger;
    private readonly MockHttpClient _mocks;

    public MockHttpMessageHandler(MockHttpClient mocks, ILogger logger)
    {
        _mocks = mocks;
        _logger = new HttpLogger(logger);
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
        CancellationToken cancellationToken)
    {
        await _logger.WriteAsync(request);
        var response = await _mocks.SendAsync(request);
        await _logger.WriteAsync(response);
        return response;
    }
}
