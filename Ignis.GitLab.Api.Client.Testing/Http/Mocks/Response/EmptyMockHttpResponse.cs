namespace Ignis.GitLab.Api.Client.Http.Mocks.Response;

internal sealed class EmptyMockHttpResponse : MockHttpResponse
{
    private EmptyMockHttpResponse()
    {
    }

    public static MockHttpResponse Instance { get; } = new EmptyMockHttpResponse();

    public override HttpContent? ToContent()
    {
        return null;
    }
}
