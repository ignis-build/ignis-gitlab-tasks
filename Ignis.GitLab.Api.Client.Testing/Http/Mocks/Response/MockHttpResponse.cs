using System.Net;

namespace Ignis.GitLab.Api.Client.Http.Mocks.Response;

public abstract class MockHttpResponse
{
    public static MockHttpResponse Empty => EmptyMockHttpResponse.Instance;

    public static MockHttpResponse Json<T>(T body) where T : class
    {
        return JsonMockHttpResponse.Create(body);
    }

    public static MockHttpResponse Fail(HttpStatusCode statusCode)
    {
        return new FailedMockHttpResponse(statusCode);
    }

    public abstract HttpContent? ToContent();

    public virtual HttpStatusCode GetResponseCode()
    {
        return HttpStatusCode.OK;
    }
}
