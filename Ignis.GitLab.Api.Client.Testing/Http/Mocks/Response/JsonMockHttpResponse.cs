using System.Net.Http.Json;

namespace Ignis.GitLab.Api.Client.Http.Mocks.Response;

internal sealed class JsonMockHttpResponse : MockHttpResponse
{
    private readonly object _body;

    private JsonMockHttpResponse(object body)
    {
        _body = body;
    }

    public static MockHttpResponse Create<T>(T body) where T : class
    {
        return new JsonMockHttpResponse(body);
    }

    public override HttpContent ToContent()
    {
        return JsonContent.Create(_body);
    }
}
