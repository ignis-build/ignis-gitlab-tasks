using System.Net;

namespace Ignis.GitLab.Api.Client.Http.Mocks.Response;

internal sealed class FailedMockHttpResponse : MockHttpResponse
{
    private readonly HttpStatusCode _statusCode;

    public FailedMockHttpResponse(HttpStatusCode statusCode)
    {
        _statusCode = statusCode;
    }

    public override HttpContent? ToContent()
    {
        return null;
    }

    public override HttpStatusCode GetResponseCode()
    {
        return _statusCode;
    }
}
